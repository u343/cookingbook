package com.u343.cookingbook.recipe_db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.u343.cookingbook.book.data.recipe_db.CookingBookDatabase
import com.u343.cookingbook.book.data.recipe_db.dao.DaoCategory
import com.u343.cookingbook.book.models.recipe_db.CategoryEntity
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RecipeDataBaseCategoryTest {

    private lateinit var categoryDao: DaoCategory
    private lateinit var dataBase: CookingBookDatabase

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        dataBase = Room.inMemoryDatabaseBuilder(
            context, CookingBookDatabase::class.java).build()
        categoryDao = dataBase.recipeCategoryDao()
    }

    @After
    fun tearDown() {
        dataBase.close()
    }

    @Test
    fun writeCategoryAndReadList() {
        val category = CategoryEntity(categoryName = "firstCategory")

        runBlocking {
            launch {
                categoryDao.insert(category)
                val listCategory = categoryDao.getAll()
                assertThat(listCategory.first(), equalTo("cat"))
            }
        }
    }
}