package com.u343.cookingbook.book.domain.application

import android.app.Application
import com.u343.cookingbook.book.di.AppComponent
import com.u343.cookingbook.book.di.CookingBookModule
import com.u343.cookingbook.book.di.DaggerAppComponent

/**
 * Кастомный [Application], нужен для инициализации даггер графа
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        initDagger()
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder()
            .cookingBookModule(CookingBookModule(applicationContext))
            .build()
    }

    companion object {
        lateinit var appComponent: AppComponent
            private set
    }
}
