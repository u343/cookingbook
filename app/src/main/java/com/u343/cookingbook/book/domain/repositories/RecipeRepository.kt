package com.u343.cookingbook.book.domain.repositories

import com.u343.cookingbook.book.models.recipe_db.CategoryEntity
import com.u343.cookingbook.book.models.recipe_db.RecipeDataEntity

/**
 * Репозиторий для взаимодействия с БД рецептов
 */
interface RecipeRepository {

    /**
     * Получить все рецепты
     *
     * @return список моделей рецептов
     */
    suspend fun getAllRecipes(): List<RecipeDataEntity>

    /**
     * Получить все рецепты по категории
     *
     * @param idCategory идентификатор категории
     * @return список моделей рецептов
     */
    suspend fun getAllRecipesByCategory(idCategory: Int): List<RecipeDataEntity>

    /**
     * Получить рецепт по еего идентификатору
     *
     * @param id идентификатор рецепта
     * @return модель рецепта
     */
    suspend fun getRecipeById(id: Int): RecipeDataEntity

    /**
     * Добавить рецепт в хранилище
     *
     * @param recipe рецепт, который необходимо добавить
     */
    suspend fun addNewRecipe(recipe: RecipeDataEntity)

    /**
     * Изменить рецепт
     *
     * @param recipe рецепт, который необходимо обновить
     */
    suspend fun updateRecipe(recipe: RecipeDataEntity)

    /**
     * Удалить рецепт
     *
     * @param recipe рецепт, который необходимо удалить
     */
    suspend fun deleteRecipe(recipe: RecipeDataEntity)

    /**
     * Получить любимые рецепты
     *
     * @return список любимых рецептов рецептов
     */
    suspend fun getFavoriteRecipes(): List<RecipeDataEntity>

    /**
     * Добавить рецепт к категории
     *
     * @param idCategory идентификатор категории
     * @param idRecipe идентификотор рецепта
     */
    suspend fun addRecipeToCategory(idCategory: Int, idRecipe: Int)

    /**
     * Удалить рецепт из категории
     *
     * @param idCategory идентификатор категории
     * @param idRecipe идентификотор рецепта
     */
    suspend fun deleteRecipeFromCategory(idCategory: Int, idRecipe: Int)

    /**
     * Получить все категории, в которых находится рецепт
     *
     * @param idRecipe идентификотор рецепта
     */
    suspend fun getCategoriesByRecipe(idRecipe: Int): List<CategoryEntity>
}
