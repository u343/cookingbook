package com.u343.cookingbook.book.presentation.main_screen.dialog

/**
 * Типы кликабельных элементов шторки
 */
enum class MainScreenBottomSheetClickType {

    /** Создание рецепта */
    RECIPE,

    /** Создание категории */
    CATEGORY;
}
