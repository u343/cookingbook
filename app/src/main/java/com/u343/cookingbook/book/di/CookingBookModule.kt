package com.u343.cookingbook.book.di

import android.content.Context
import androidx.room.Room
import com.u343.cookingbook.book.data.recipe_db.CookingBookDatabase
import com.u343.cookingbook.book.data.repositories.CategoryRepositoryImpl
import com.u343.cookingbook.book.data.repositories.RecipeRepositoryImpl
import com.u343.cookingbook.book.domain.interactors.MainScreenInteractor
import com.u343.cookingbook.book.domain.observer.MainScreenObserverImpl
import com.u343.cookingbook.book.domain.repositories.CategoryRepository
import com.u343.cookingbook.book.domain.repositories.RecipeRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CookingBookModule(private val applicationContex: Context) {

    @Provides
    fun provideApllicationContext(): Context = applicationContex

    @Provides
    @Singleton
    fun provideRecipesDatabase(context: Context): CookingBookDatabase {
        return Room.databaseBuilder(
            context,
            CookingBookDatabase::class.java,
            CookingBookDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun provideCategoryRepository(database: CookingBookDatabase): CategoryRepository {
        return CategoryRepositoryImpl(database.recipeCategoryDao())
    }

    @Provides
    @Singleton
    fun provideRecipeRepository(database: CookingBookDatabase): RecipeRepository {
        return RecipeRepositoryImpl(database.recipesDao(), database.categoryRecipeCrossRefDao())
    }

    @Provides
    @Singleton
    fun provideMainScreenInteractor(
        categoryRepository: CategoryRepository,
        recipeRepository: RecipeRepository
    ): MainScreenInteractor {
        return MainScreenInteractor(
            categoryRepository,
            recipeRepository,
            MainScreenObserverImpl()
        )
    }
}
