package com.u343.cookingbook.book.models.recipe_db

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

/**
 * Модель категорий с рецептами, которые хранятся в этой категории
 *
 * @property category модель категории
 * @property recipes рецепты, относящиеся к этой категории
 */
data class CategoryWithRecipes(
    @Embedded val category: CategoryEntity,
    @Relation(
        parentColumn = "categoryId",
        entityColumn = "recipeId",
        associateBy = Junction(CategoryRecipeCrossRef::class)
    )
    val recipes: List<RecipeDataEntity>
)
