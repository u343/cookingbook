package com.u343.cookingbook.book.domain.interactors

import com.u343.cookingbook.book.domain.observer.MainScreenObserver
import com.u343.cookingbook.book.domain.observer.MainScreenObserverEvent
import com.u343.cookingbook.book.domain.repositories.CategoryRepository
import com.u343.cookingbook.book.domain.repositories.RecipeRepository
import com.u343.cookingbook.book.models.recipe_db.CategoryEntity
import com.u343.cookingbook.book.models.recipe_db.RecipeDataEntity

/**
 * Интерактор главного экрана
 *
 * @param categoryRepository Репозиторий категорий
 * @param recipeRepository Репозиторий рецептов
 * @param mainScreenObserver Наблюдатель для отслеживания изменений в БД
 */
class MainScreenInteractor(
    private val categoryRepository: CategoryRepository,
    private val recipeRepository: RecipeRepository,
    private val mainScreenObserver: MainScreenObserver
) {

    fun subscribeOnUpdates(subscriber: MainScreenObserverEvent) {
        mainScreenObserver.subscribe(subscriber)
    }

    /**
     * Добавляем категорию в БД и оповещаем подписчиков [MainScreenObserver] об обновлении
     */
    suspend fun addNewCategory(category: CategoryEntity) {
        categoryRepository.addNewCategory(category)
        mainScreenObserver.update()
    }

    suspend fun getAllCategories(): List<CategoryEntity> = categoryRepository.getAllCategories()

    suspend fun getAllRecipes(): List<RecipeDataEntity> = recipeRepository.getAllRecipes()

    suspend fun getFavoriteRecipes(): List<RecipeDataEntity> = recipeRepository.getFavoriteRecipes()
}
