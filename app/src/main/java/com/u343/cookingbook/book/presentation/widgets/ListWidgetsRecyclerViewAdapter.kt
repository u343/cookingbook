package com.u343.cookingbook.book.presentation.widgets

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem
import com.u343.cookingbook.book.presentation.widgets.list_widgets.ListWidget

/**
 * Адаптер для [RecyclerView] главного экрана со списком категорий и рецептов
 *
 * @param listItemWidgets View элементы которые будут отображаться в [RecyclerView]
 */
class ListWidgetsRecyclerViewAdapter(
    private val listItemWidgets: List<ListWidget<*, *>>
) : ListAdapter<ListItem, BaseViewHolder<View, ListItem>>(ListWidgetDiffUtil(listItemWidgets)) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<View, ListItem> {
        val inflater = LayoutInflater.from(parent.context)
        return listItemWidgets.find { it.getLayoutId() == viewType }
            ?.getViewHolder(inflater, parent)
            ?.let { it as BaseViewHolder<View, ListItem> }
            ?: throw IllegalArgumentException("View type not found: $viewType")
    }

    override fun onBindViewHolder(holder: BaseViewHolder<View, ListItem>, position: Int) {
        holder.onBind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return listItemWidgets.find { it.isRelativeItem(item) }
            ?.getLayoutId()
            ?: throw IllegalArgumentException("View type not found: $item")
    }
}
