package com.u343.cookingbook.book.domain.observer

/**
 * Наблюдатель главного экрана. Используется для слежки за обновлениями данных в БД,
 * чтобы фрагменты могли своевременно обноляться
 */
interface MainScreenObserver {

    /**
     * Подписаться на уведомления. Каждый подписчик должен реализовать интерфейс
     * [MainScreenObserverEvent], через него и будет совершаться действие при обновлении
     *
     * @param subscriber Подписчик
     */
    fun subscribe(subscriber: MainScreenObserverEvent)

    /**
     * Сигнал что данные обновлены и нужно соощить об этом подписчикам
     */
    fun update()
}