package com.u343.cookingbook.book.models.presentation.list_item_screen

/**
 * Режимы в которых может работать экран со списком категорий/рецептов,
 * в зависимости от типа логика работа экрана будет меняться
 */
enum class ListItemScreenType {

    /** Отобразить все категории */
    ALL_CATEGORIES,

    /** Отобразить рецепты отдельной категории */
    CATEGORY_CONTENT;
}
