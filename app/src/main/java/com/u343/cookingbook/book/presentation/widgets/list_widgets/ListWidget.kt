package com.u343.cookingbook.book.presentation.widgets.list_widgets

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import com.u343.cookingbook.book.presentation.widgets.BaseViewHolder
import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem

/**
 * Интрефейс для виджета recyclerView
 *
 * @param V View к которому будет привязана эта логика
 * @param I Всмогательный класс в котором хранятся данные, необходимые для постороения виджета
 */
interface ListWidget<V : View, I : ListItem> {

    /**
     * Предназначены ли данные из класса [ListItem] для этого [ListWidget]
     */
    fun isRelativeItem(item: ListItem): Boolean

    @LayoutRes
    fun getLayoutId(): Int

    fun getViewHolder(
        layoutInflater: LayoutInflater,
        parent: ViewGroup
    ): BaseViewHolder<V, I>

    /**
     * Необходим для связи с ListAdapter
     */
    fun getDiffUtil(): DiffUtil.ItemCallback<I>
}
