package com.u343.cookingbook.book.presentation.recipes.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.DialogFragment
import com.u343.cookingbook.R
import com.u343.cookingbook.book.models.recipe_db.CategoryEntity

/**
 * Диалог смены категорий
 *
 * Диалог можно запускать только из фргамента
 *
 * @property categoriesList список доступных категорий
 */
class ChangeCategoryDialog(
    private val categoriesList: List<CategoryEntity>
) : DialogFragment() {

    private var listener: ChangeCategoryListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)

        parentFragment?.let { fragment ->

            listener = fragment as? ChangeCategoryListener
            Log.d("dialogTest", "parentFragment not null")
        }
        Log.d("dialogTest", "parentFragment mb null")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val categoriesArray = categoriesList.map { it.categoryName }.toTypedArray()

        return if (listener != null) {
            Log.d("dialogTest", "listener not null")
            requireActivity().run {
                val builder = AlertDialog.Builder(this)
                builder.setTitle(R.string.category_dialog_chose_category)
                    .setItems(categoriesArray) { dialog, which ->
                        listener?.onChangeDialogItemClick(categoriesList[which].categoryId)
                    }
                builder.create()
            }
        } else {
            Log.d("dialogTest", "listener null")
            super.onCreateDialog(savedInstanceState)
        }
    }

    companion object {
        const val TAG = "ChangeCategoryDialog"
    }
}
