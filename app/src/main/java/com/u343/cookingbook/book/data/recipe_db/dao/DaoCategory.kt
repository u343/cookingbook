package com.u343.cookingbook.book.data.recipe_db.dao

import androidx.room.*
import com.u343.cookingbook.book.models.recipe_db.CategoryEntity
import com.u343.cookingbook.book.models.recipe_db.CategoryWithRecipes

/**
 * Объект для доступа к данным о категориях рецептов
 */
@Dao
interface DaoCategory {

    @Query("SELECT * FROM ${CategoryEntity.TABLE_NAME}")
    suspend fun getAll(): List<CategoryEntity>

    @Query("SELECT * FROM ${CategoryEntity.TABLE_NAME} WHERE categoryId LIKE :idCategory  LIMIT 1")
    suspend fun getCategoryById(idCategory: Int): CategoryEntity

    @Update
    suspend fun update(recipe: CategoryEntity)

    @Delete
    suspend fun delete(category: CategoryEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(category: CategoryEntity)
}
