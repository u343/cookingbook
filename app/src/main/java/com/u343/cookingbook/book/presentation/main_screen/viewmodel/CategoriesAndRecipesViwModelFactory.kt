package com.u343.cookingbook.book.presentation.main_screen.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.u343.cookingbook.book.domain.interactors.MainScreenInteractor

class CategoriesAndRecipesViwModelFactory(
    private val interactor: MainScreenInteractor
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CategoriesAndRecipesViewModel(interactor) as T
    }
}
