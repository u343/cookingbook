package com.u343.cookingbook.book.models.presentation.main_screen

/**
 * На экране категорий и рецептов в зависимости от контента меняется текст тулбара,
 * данный enum позволяет фиксировать какой сейчас текст отображен
 */
enum class CategoriesAndRecipeToolbarContentType {
    CATEGORY,
    RECIPE;
}
