package com.u343.cookingbook.book.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.u343.cookingbook.R
import com.u343.cookingbook.book.models.presentation.list_item_screen.ListItemScreenType
import com.u343.cookingbook.book.presentation.list_item_screen.ListItemsFragment
import com.u343.cookingbook.book.presentation.main_screen.view.MainScreenContainerFragment
import com.u343.cookingbook.book.presentation.photo_screen.CameraScreenFragment
import com.u343.cookingbook.book.presentation.recipes.RecipesListFragment
import com.u343.cookingbook.book.presentation.recipes.creation.CreationRecipeFragment
import com.u343.cookingbook.book.presentation.recipes.info.InfoRecipeFragment
import com.u343.cookingbook.book.presentation.router.CookingBookRouter

/**
 * Главное активити приложения
 */
class MainActivity : AppCompatActivity(), CookingBookRouter {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            goToMainScreen()
        }
    }

    override fun goToMainScreen() {
        showFragment(MainScreenContainerFragment.newInstance(), false, MainScreenContainerFragment.TAG)
    }

    override fun goToRecipesScreen(idCategory: Int) {
        showFragment(RecipesListFragment.newInstance(idCategory), true, RecipesListFragment.TAG)
    }

    override fun goToRecipeCreationScreen(idCategory: Int?, idRecipe: Int?) {
        showFragment(CreationRecipeFragment.newInstance(idCategory, idRecipe), true, CreationRecipeFragment.TAG)
    }

    override fun goToRecipeInfoScreen(idRecipe: Int) {
        showFragment(InfoRecipeFragment.newInstance(idRecipe), true, InfoRecipeFragment.TAG)
    }

    override fun goToCameraScreen() {
        showFragment(CameraScreenFragment.newInstance(), true, CameraScreenFragment.TAG)
    }

    override fun goToCategoriesScreen(screenType: ListItemScreenType, idCategory: Int?) {
        showFragment(ListItemsFragment.newInstance(screenType, idCategory), true, ListItemsFragment.TAG)
    }

    private fun showFragment(fragment: Fragment, toBackStack: Boolean, tag: String) {
        val fragmentManager = supportFragmentManager

        val fragmentTransaction = fragmentManager.beginTransaction()
        val newFragment = fragmentManager.findFragmentByTag(tag) ?: fragment
        fragmentTransaction.replace(R.id.content_container, newFragment, tag)
        if (toBackStack) {
            fragmentTransaction.addToBackStack(null)
        }
        fragmentTransaction.commit()
    }
}
