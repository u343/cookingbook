package com.u343.cookingbook.book.models.presentation.main_screen

import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem

/**
 * Модель, возвращаемая вью моделью, для главного экрана
 *
 * @property listItems список элементов для recycler view
 * @property recipesCount количество рецептов
 * @property categoriesCount количество категорий
 */
data class CategoriesAndRecipeViewModelData(
    val listItems: List<ListItem>,
    val recipesCount: Int,
    val categoriesCount: Int
)
