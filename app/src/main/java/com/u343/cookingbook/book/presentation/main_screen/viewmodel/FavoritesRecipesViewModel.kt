package com.u343.cookingbook.book.presentation.main_screen.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.u343.cookingbook.book.domain.interactors.MainScreenInteractor
import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.RecipeListItem
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/**
 * [ViewModel] экрана с любимыми рецептами
 *
 * @param interactor Интерактор главного экрана
 */
class FavoritesRecipesViewModel(
    private val interactor: MainScreenInteractor
) : ViewModel() {

    /**
     * [LiveData] со списком любимых рецептов
     */
    private val _favoriteRecipesList = MutableLiveData<List<ListItem>>()
    val favoriteRecipesList: LiveData<List<ListItem>>
        get() = _favoriteRecipesList

    fun loadData() {
        runBlocking {
            launch {
                _favoriteRecipesList.value = interactor.getFavoriteRecipes().map { recipeData ->
                    RecipeListItem(
                        recipeId = recipeData.recipeId,
                        recipeName = recipeData.recipeName,
                        image = null,
                        isFavorite = recipeData.isFavorite
                    )
                }.reversed()
            }
        }
    }
}
