package com.u343.cookingbook.book.presentation.widgets.list_widgets

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import com.u343.cookingbook.R
import com.u343.cookingbook.book.models.presentation.list_widgets.CategoryListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.LabelListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem
import com.u343.cookingbook.book.presentation.widgets.BaseViewHolder
import com.u343.cookingbook.book.presentation.widgets.list_widgets.ListWidget

/**
 * Некликабельный текстовый виджет
 */
class LabelListWidget : ListWidget<View, LabelListItem> {

    override fun isRelativeItem(item: ListItem): Boolean = item is LabelListItem

    override fun getLayoutId(): Int = R.layout.list_item_label

    override fun getViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup): BaseViewHolder<View, LabelListItem> {
        val view = layoutInflater
            .inflate(R.layout.list_item_label, parent, false)

        return LabelViewHolder(view)
    }

    override fun getDiffUtil(): DiffUtil.ItemCallback<LabelListItem> = diffUtil

    private val diffUtil = object : DiffUtil.ItemCallback<LabelListItem>() {
        override fun areItemsTheSame(oldItem: LabelListItem, newItem: LabelListItem) =
            oldItem.title == newItem.title

        override fun areContentsTheSame(oldItem: LabelListItem, newItem: LabelListItem) = oldItem == newItem
    }
}

class LabelViewHolder(
    view: View
) : BaseViewHolder<View, LabelListItem>(view) {

    private val titleView: TextView = view.findViewById(R.id.list_item_label_title)
    private val valueView: TextView = view.findViewById(R.id.list_item_label_value)

    override fun onBind(item: LabelListItem) {
        item.run {
            titleView.text = title
            valueView.text = value
        }
    }
}
