package com.u343.cookingbook.book.presentation.list_item_screen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.u343.cookingbook.book.domain.repositories.CategoryRepository
import com.u343.cookingbook.book.domain.repositories.RecipeRepository

class ListItemsViewModelFactory(
    private val categoryRepository: CategoryRepository,
    private val recipeRepository: RecipeRepository
): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ListItemsViewModel(categoryRepository, recipeRepository) as T
    }
}