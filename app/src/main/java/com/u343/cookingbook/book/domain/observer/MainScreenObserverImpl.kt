package com.u343.cookingbook.book.domain.observer

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class MainScreenObserverImpl : MainScreenObserver {

    private val subscribersList: MutableList<MainScreenObserverEvent> = ArrayList()

    override fun subscribe(subscriber: MainScreenObserverEvent) {
        if (subscribersList.contains(subscriber).not()) {
            subscribersList.add(subscriber)
        }
    }

    override fun update() {
        runBlocking {
            launch {
                callToSubscribers()
            }
        }
    }

    private fun callToSubscribers() {
        for (i in subscribersList) {
            i.event()
        }
    }
}
