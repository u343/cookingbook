package com.u343.cookingbook.book.presentation.main_screen.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.u343.cookingbook.R
import com.u343.cookingbook.book.domain.application.App
import com.u343.cookingbook.book.domain.interactors.MainScreenInteractor
import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.RecipeListItem
import com.u343.cookingbook.book.presentation.widgets.ListWidgetsRecyclerViewAdapter
import com.u343.cookingbook.book.presentation.main_screen.viewmodel.FavoritesRecipesViewModel
import com.u343.cookingbook.book.presentation.main_screen.viewmodel.FavoritesRecipesViewModelFactory
import com.u343.cookingbook.book.presentation.router.CookingBookRouter
import com.u343.cookingbook.book.presentation.utils.attachFab
import com.u343.cookingbook.book.presentation.widgets.list_widgets.RecipeListWidget
import com.u343.cookingbook.book.presentation.widgets.list_widgets.RecipeListWidgetAction
import java.lang.ref.WeakReference
import javax.inject.Inject

class FavoritesRecipesFragment : Fragment(), RecipeListWidgetAction {

    @Inject
    lateinit var interactor: MainScreenInteractor

    private lateinit var recipesRecyclerView: RecyclerView

    private lateinit var favoriteRecipesRecyclerViewAdapter: ListWidgetsRecyclerViewAdapter
    private lateinit var router: WeakReference<CookingBookRouter>
    private lateinit var favoritesRecipesViewModel: FavoritesRecipesViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val activity = requireActivity()
        if (activity is CookingBookRouter) {
            router = WeakReference(activity)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main_screen_favorites_recipes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        App.appComponent.inject(fragment = this@FavoritesRecipesFragment)

        favoritesRecipesViewModel = ViewModelProvider(
            this, FavoritesRecipesViewModelFactory(
                interactor
            )
        ).get(FavoritesRecipesViewModel::class.java)

        initViews(view)
        initObservers()

        favoritesRecipesViewModel.loadData()
    }

    override fun recipeListWidgetClick(item: RecipeListItem) {
        router.get()?.goToRecipeInfoScreen(item.recipeId)
    }

    private fun initViews(view: View) {
        recipesRecyclerView = view.findViewById(R.id.main_screen_favorite_recipes_recycler_view)
        setUpRecyclerView()
    }

    private fun initObservers() {
        favoritesRecipesViewModel.favoriteRecipesList.observe(viewLifecycleOwner) { data ->
            setDataToRecyclerView(data)
        }
    }

    private fun setDataToRecyclerView(data: List<ListItem>) {
        favoriteRecipesRecyclerViewAdapter.submitList(data)
    }

    /**
     * Виджеты которые бутут отображены в recyclerView
     */
    private fun getWidgets() = listOf(
        RecipeListWidget(this)
    )

    private fun setUpRecyclerView() {
        favoriteRecipesRecyclerViewAdapter = ListWidgetsRecyclerViewAdapter(getWidgets())
        recipesRecyclerView.adapter = favoriteRecipesRecyclerViewAdapter
        bindRecyclerViewWithAddButton()
    }

    /**
     * Связывает [RecyclerView] данного экрана с кнопкой "добавить новый элемент" с экрана
     * контейнера [MainScreenContainerFragment]. Во время прокрутки списка кнопка будет скрываться
     */
    private fun bindRecyclerViewWithAddButton() {
        recipesRecyclerView.attachFab((parentFragment as MainScreenContainerFragment).addButton)
    }

    companion object {
        const val TAG = "FavoritesRecipesFragment"

        /**
         * Фабричный метод для получению фрагмента
         */
        fun newInstance() = FavoritesRecipesFragment()
    }
}
