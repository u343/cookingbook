package com.u343.cookingbook.book.presentation.main_screen.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.u343.cookingbook.R
import com.u343.cookingbook.book.domain.application.App
import com.u343.cookingbook.book.domain.interactors.MainScreenInteractor
import com.u343.cookingbook.book.models.presentation.list_widgets.CategoryListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.RecipeListItem
import com.u343.cookingbook.book.models.presentation.main_screen.CategoriesAndRecipeToolbarContentType
import com.u343.cookingbook.book.models.presentation.main_screen.CategoriesAndRecipeViewModelData
import com.u343.cookingbook.book.presentation.widgets.ListWidgetsRecyclerViewAdapter
import com.u343.cookingbook.book.presentation.main_screen.viewmodel.CategoriesAndRecipesViewModel
import com.u343.cookingbook.book.presentation.main_screen.viewmodel.CategoriesAndRecipesViwModelFactory
import com.u343.cookingbook.book.presentation.router.CookingBookRouter
import com.u343.cookingbook.book.presentation.utils.attachFab
import com.u343.cookingbook.book.presentation.widgets.list_widgets.CategoryListWidget
import com.u343.cookingbook.book.presentation.widgets.list_widgets.CategoryListWidgetAction
import com.u343.cookingbook.book.presentation.widgets.list_widgets.LabelListWidget
import com.u343.cookingbook.book.presentation.widgets.list_widgets.RecipeListWidget
import com.u343.cookingbook.book.presentation.widgets.list_widgets.RecipeListWidgetAction
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Фрагмент главного экрана
 *
 * Отображает основную информацию, список категорий и рецептов
 */
class CategoriesAndRecipesFragment : Fragment(), RecipeListWidgetAction, CategoryListWidgetAction {

    @Inject
    lateinit var interactor: MainScreenInteractor

    private lateinit var mainRecyclerView: RecyclerView

    private lateinit var allRecipesRecyclerViewAdapter: ListWidgetsRecyclerViewAdapter
    private lateinit var router: WeakReference<CookingBookRouter>
    private lateinit var viewModelData: CategoriesAndRecipeViewModelData
    private lateinit var categoriesAndRecipesViewModel: CategoriesAndRecipesViewModel

    private var toolBarTextType = CategoriesAndRecipeToolbarContentType.CATEGORY

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val activity = requireActivity()
        if (activity is CookingBookRouter) {
            router = WeakReference(activity)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main_screen_all_items, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        App.appComponent.inject(fragment = this@CategoriesAndRecipesFragment)

        categoriesAndRecipesViewModel = ViewModelProvider(
            this, CategoriesAndRecipesViwModelFactory(
                interactor
            )
        ).get(CategoriesAndRecipesViewModel::class.java)

        initViews(view)
        initObservers()

        categoriesAndRecipesViewModel.loadData()
    }

    override fun recipeListWidgetClick(item: RecipeListItem) {
        router.get()?.goToRecipeInfoScreen(item.recipeId)
    }

    override fun categoryListWidgetClick(item: CategoryListItem) {
        router.get()?.goToRecipesScreen(item.categoryId)
    }

    private fun initViews(view: View) {
        mainRecyclerView = view.findViewById(R.id.all_recipes_recycler_view)
        setUpRecyclerView()
    }

    private fun initObservers() {
        categoriesAndRecipesViewModel.recyclerViewData.observe(viewLifecycleOwner) { data ->
            Log.d("bondarevTest", "initObservers ${data.categoriesCount}")
            viewModelData = data
            setDataToRecyclerView(data.listItems)
            changeToolbarText(
                resources.getString(R.string.main_screen_categories) + " ${viewModelData.categoriesCount}"
            )
        }
    }

    private fun setUpRecyclerView() {
        allRecipesRecyclerViewAdapter = ListWidgetsRecyclerViewAdapter(getWidgets())
        allRecipesRecyclerViewAdapter.setHasStableIds(false)
        mainRecyclerView.adapter = allRecipesRecyclerViewAdapter

        bindRecyclerViewWithAddButton()
        bindRecyclerViewWithToolbar()
    }

    private fun changeToolbarText(newText: String) {
        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.let { supportBar ->
                supportBar.title = newText
            }
        }
    }

    private fun setDataToRecyclerView(data: List<ListItem>) {
        allRecipesRecyclerViewAdapter.submitList(data)
    }

    /**
     * Связывает [RecyclerView] данного экрана с кнопкой "добавить новый элемент" с экрана
     * контейнера [MainScreenContainerFragment]. Во время прокрутки списка кнопка будет скрываться
     */
    private fun bindRecyclerViewWithAddButton() {
        mainRecyclerView.attachFab((parentFragment as MainScreenContainerFragment).addButton)
    }

    /**
     * Связывает [RecyclerView] данного экрана с тулбаром.
     * Во время прокрутки списка будет меняться текст тулбара
     */
    private fun bindRecyclerViewWithToolbar() {
        val linearLayoutManager = mainRecyclerView.layoutManager as LinearLayoutManager
        mainRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (::viewModelData.isInitialized) {
                    val first = linearLayoutManager.findFirstCompletelyVisibleItemPosition()
                    if (toolBarTextType != CategoriesAndRecipeToolbarContentType.RECIPE
                        && (viewModelData.listItems[first] as? RecipeListItem) != null
                    ) {
                        changeToolbarText(
                            resources.getString(R.string.main_screen_recipes) + " ${viewModelData.recipesCount}"
                        )
                        toolBarTextType = CategoriesAndRecipeToolbarContentType.RECIPE
                    } else if (toolBarTextType != CategoriesAndRecipeToolbarContentType.CATEGORY
                        && (viewModelData.listItems[first] as? CategoryListItem) != null
                    ) {
                        changeToolbarText(
                            resources.getString(R.string.main_screen_categories) + " ${viewModelData.categoriesCount}"
                        )
                        toolBarTextType = CategoriesAndRecipeToolbarContentType.CATEGORY
                    }
                }
            }
        })
    }

    /**
     * Виджеты которые бутут отображены в recyclerView
     */
    private fun getWidgets() = listOf(
        CategoryListWidget(this),
        LabelListWidget(),
        RecipeListWidget(this)
    )

    companion object {
        const val TAG = "AllRecipesFragment"

        /**
         * Фабричный метод для получению фрагмента
         */
        fun newInstance() = CategoriesAndRecipesFragment()
    }
}
