package com.u343.cookingbook.book.data.repositories

import com.u343.cookingbook.book.data.recipe_db.dao.DaoCategoryRecipeCrossRef
import com.u343.cookingbook.book.data.recipe_db.dao.DaoRecipe
import com.u343.cookingbook.book.domain.repositories.RecipeRepository
import com.u343.cookingbook.book.models.recipe_db.CategoryEntity
import com.u343.cookingbook.book.models.recipe_db.CategoryRecipeCrossRef
import com.u343.cookingbook.book.models.recipe_db.RecipeDataEntity

/**
 * Реализация репозитория для взаимодействия с БД рецептов
 *
 * @property recipeDao ДАО для взаимодействия с БД рецептов
 */
class RecipeRepositoryImpl(
    private val recipeDao: DaoRecipe,
    private val crossRefDao: DaoCategoryRecipeCrossRef
) : RecipeRepository {

    override suspend fun getAllRecipes(): List<RecipeDataEntity> = recipeDao.getAll().reversed()

    override suspend fun getAllRecipesByCategory(idCategory: Int): List<RecipeDataEntity> =
        recipeDao.getListCategoriesWithRecipes()
            .find { data -> data.category.categoryId == idCategory }?.recipes?.reversed() ?: emptyList()

    override suspend fun getRecipeById(id: Int): RecipeDataEntity = recipeDao.getRecipeById(id)

    override suspend fun addNewRecipe(recipe: RecipeDataEntity) {
        recipeDao.insert(recipe)
    }

    override suspend fun updateRecipe(recipe: RecipeDataEntity) {
        recipeDao.update(recipe)
    }

    override suspend fun deleteRecipe(recipe: RecipeDataEntity) {
        recipeDao.delete(recipe)
    }

    override suspend fun getFavoriteRecipes(): List<RecipeDataEntity> = recipeDao.getFavoriteRecipes().reversed()

    override suspend fun addRecipeToCategory(idCategory: Int, idRecipe: Int) {
        crossRefDao.insert(
            CategoryRecipeCrossRef(idCategory, idRecipe)
        )
    }

    override suspend fun deleteRecipeFromCategory(idCategory: Int, idRecipe: Int) {
        crossRefDao.delete(
            CategoryRecipeCrossRef(idCategory, idRecipe)
        )
    }

    override suspend fun getCategoriesByRecipe(idRecipe: Int): List<CategoryEntity> =
        recipeDao.getListRecipesWithCategories()
            .find { data -> data.recipe.recipeId == idRecipe }?.categories ?: emptyList()
}
