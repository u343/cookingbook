package com.u343.cookingbook.book.presentation.widgets

import androidx.recyclerview.widget.DiffUtil
import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem
import com.u343.cookingbook.book.presentation.widgets.list_widgets.ListWidget

/**
 * Реализация [DiffUtil.ItemCallback] для виджетов списка.
 * Необходим для описания правил сравнеиния элементов при выхове
 */
class ListWidgetDiffUtil(
    private val listItemWidgets: List<ListWidget<*, *>>
) : DiffUtil.ItemCallback<ListItem>() {

    override fun areItemsTheSame(oldItem: ListItem, newItem: ListItem): Boolean {
        if (oldItem::class != newItem::class) return false

        return getItemCallback(oldItem).areItemsTheSame(oldItem, newItem)
    }

    override fun areContentsTheSame(oldItem: ListItem, newItem: ListItem): Boolean {
        if (oldItem::class != newItem::class) return false

        return getItemCallback(oldItem).areContentsTheSame(oldItem, newItem)
    }

    /**
     * Возвращает реализацию [DiffUtil.ItemCallback] для конкретного виджета
     */
    private fun getItemCallback(
        item: ListItem
    ): DiffUtil.ItemCallback<ListItem> = listItemWidgets.find { it.isRelativeItem(item) }
        ?.getDiffUtil()
        ?.let { it as DiffUtil.ItemCallback<ListItem> }
        ?: throw IllegalStateException("DiffUtil not found for $item")
}
