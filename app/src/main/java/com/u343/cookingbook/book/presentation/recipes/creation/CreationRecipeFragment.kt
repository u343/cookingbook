package com.u343.cookingbook.book.presentation.recipes.creation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.u343.cookingbook.R
import com.u343.cookingbook.book.domain.application.App
import com.u343.cookingbook.book.domain.repositories.RecipeRepository
import com.u343.cookingbook.book.models.recipe_db.RecipeDataEntity
import com.u343.cookingbook.book.presentation.router.CookingBookRouter
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Фрагмент создания/редактирования рецепта
 */
class CreationRecipeFragment : Fragment() {

    @Inject
    lateinit var recipeRepository: RecipeRepository

    private lateinit var creationRecipeViewModel: CreationRecipeViewModel
    private lateinit var currentRecipeDataEntity: RecipeDataEntity
    private lateinit var router: WeakReference<CookingBookRouter>

    private var categoryRecipeId: Int? = null
    private var isEditMode: Boolean = false

    private lateinit var titleEditText: EditText
    private lateinit var descriptionEditText: EditText
    private lateinit var ingredientsEditText: EditText
    private lateinit var recipeEditText: EditText
    private lateinit var addButton: Button
    private lateinit var cameraButton: Button

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val activity = requireActivity()
        if (activity is CookingBookRouter) {
            router = WeakReference(activity)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_creation_recipe, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        App.appComponent.inject(fragment = this@CreationRecipeFragment)

        creationRecipeViewModel = CreationRecipeViewModel(recipeRepository)

        initViews(view)
        initObservers()
        setUpToolbar()
        checkModeOfScreen()
        addButton.setOnClickListener { createRecipe() }
        cameraButton.setOnClickListener { router.get()?.goToCameraScreen() }

        categoryRecipeId = arguments?.getInt(ARG_CATEGORY_KEY)
    }

    private fun initViews(view: View) {
        titleEditText = view.findViewById(R.id.creation_recipe_edit_title)
        descriptionEditText = view.findViewById(R.id.creation_recipe_edit_description)
        ingredientsEditText = view.findViewById(R.id.creation_recipe_edit_ingredients)
        recipeEditText = view.findViewById(R.id.creation_recipe_edit_recipe)
        addButton = view.findViewById(R.id.creation_recipe_add_button)
        cameraButton = view.findViewById(R.id.creation_recipe_camera_button)
    }

    private fun initObservers() {
        creationRecipeViewModel.currentRecipe.observe(viewLifecycleOwner) { recipeData ->
            currentRecipeDataEntity = recipeData
            showEditableRecipeData(recipeData)
        }
    }

    private fun setUpToolbar() {
        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            requireActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Проверяем для какого действия был открыт экран, для редактировани или для создания нового рецепта
     * и устанавливает значение переменной [isEditMode]
     */
    private fun checkModeOfScreen() {
        arguments?.let {
            val idRecipe = it.getInt(ARG_RECIPE_KEY, -1)
            if (idRecipe != -1) {
                isEditMode = true
                creationRecipeViewModel.getRecipeById(idRecipe)
            }
        }
    }

    private fun showEditableRecipeData(recipeData: RecipeDataEntity) {
        titleEditText.setText(recipeData.recipeName)
        descriptionEditText.setText(recipeData.description)
        ingredientsEditText.setText(recipeData.ingredients)
        recipeEditText.setText(recipeData.recipe)
        addButton.text = resources.getString(R.string.creation_recipe_create_button_update)
    }

    private fun createRecipe() {

        if (titleEditText.text.isNullOrBlank()
            || descriptionEditText.text.isNullOrBlank()
            || ingredientsEditText.text.isNullOrBlank()
            || recipeEditText.text.isNullOrBlank()
        ) {
            showWarningMessage()
        } else {

            if (isEditMode) updateRecipe() else filledNewRecipeData()
            closeThisFragment()
        }
    }

    private fun filledNewRecipeData() {
        categoryRecipeId?.let { categoryRecipeId ->
            creationRecipeViewModel.setRecipe(
                RecipeDataEntity(
                    recipeName = titleEditText.text.toString(),
                    description = descriptionEditText.text.toString(),
                    ingredients = ingredientsEditText.text.toString(),
                    recipe = recipeEditText.text.toString()
                )
            )
        } ?: run {
            Toast.makeText(
                requireContext(),
                resources.getString(R.string.creation_recipe_category_error),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun updateRecipe() {
        creationRecipeViewModel.updateRecipe(
            currentRecipeDataEntity.apply {
                recipeName = titleEditText.text.toString()
                description = descriptionEditText.text.toString()
                ingredients = ingredientsEditText.text.toString()
                recipe = recipeEditText.text.toString()
            }
        )
    }

    private fun showWarningMessage() {
        val warningText = when {
            titleEditText.text.isNullOrBlank() -> resources.getString(R.string.creation_recipe_missing_title)
            descriptionEditText.text.isNullOrBlank() -> resources.getString(R.string.creation_recipe_missing_description)
            ingredientsEditText.text.isNullOrBlank() -> resources.getString(R.string.creation_recipe_missing_ingredients)
            recipeEditText.text.isNullOrBlank() -> resources.getString(R.string.creation_recipe_missing_recipe)
            else -> {
                throw  Exception("CreationRecipeFragment выполнению действия мешает неизвестный параметр")
            }
        }

        Toast.makeText(
            requireContext(),
            warningText,
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun closeThisFragment() {
        requireActivity().supportFragmentManager.popBackStack()
    }

    companion object {
        const val TAG = "CreationRecipeFragment"
        private const val ARG_CATEGORY_KEY = "id_category_extra_creation"
        private const val ARG_RECIPE_KEY = "id_recipe_extra_creation"

        /**
         * Фабричный метод для получению фрагмента
         *
         * @param idCategory идентификатор категории, к которой относится рецепт
         * @param idRecipe идентификатор рецепта, который нужно изменить
         */
        fun newInstance(idCategory: Int? = null, idRecipe: Int? = null) = CreationRecipeFragment().apply {
            arguments = Bundle().apply {
                idCategory?.let { putInt(ARG_CATEGORY_KEY, idCategory) }
                idRecipe?.let { putInt(ARG_RECIPE_KEY, idRecipe) }
            }
        }
    }
}
