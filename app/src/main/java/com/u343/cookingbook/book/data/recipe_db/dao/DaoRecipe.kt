package com.u343.cookingbook.book.data.recipe_db.dao

import androidx.room.*
import com.u343.cookingbook.book.models.recipe_db.CategoryEntity
import com.u343.cookingbook.book.models.recipe_db.CategoryWithRecipes
import com.u343.cookingbook.book.models.recipe_db.RecipeDataEntity
import com.u343.cookingbook.book.models.recipe_db.RecipeWithCategories

/**
 * Объект для доступа к данным о рецептах
 */
@Dao
interface DaoRecipe {
    @Query("SELECT * FROM ${RecipeDataEntity.TABLE_NAME}")
    suspend fun getAll(): List<RecipeDataEntity>

    @Query("SELECT * FROM ${RecipeDataEntity.TABLE_NAME} WHERE recipeId LIKE :recipeId  LIMIT 1")
    suspend fun getRecipeById(recipeId: Int): RecipeDataEntity

    @Transaction
    @Query("SELECT * FROM ${RecipeDataEntity.TABLE_NAME}")
    suspend fun getListRecipesWithCategories(): List<RecipeWithCategories>

    @Transaction
    @Query("SELECT * FROM ${CategoryEntity.TABLE_NAME}")
    suspend fun getListCategoriesWithRecipes(): List<CategoryWithRecipes>

    /**
     * Получить все рецепты у которых favorite_status == true
     *
     * SQL Lite хранит логические данные как цифры 1 (true) и 0 (false)
     */
    @Query("SELECT * FROM ${RecipeDataEntity.TABLE_NAME} WHERE favorite_status = 1")
    suspend fun getFavoriteRecipes(): List<RecipeDataEntity>

    @Update
    suspend fun update(recipe: RecipeDataEntity)

    @Delete
    suspend fun delete(recipe: RecipeDataEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(recipe: RecipeDataEntity)
}
