package com.u343.cookingbook.book.models.recipe_db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Модель категорий
 *
 * @property categoryId id категрии, параметр проставляется автоматически, не нужно его явно передавать
 * @property categoryName Название категории
 */
@Entity
data class CategoryEntity(
    @PrimaryKey(autoGenerate = true) var categoryId: Int = 0,
    @ColumnInfo(name = "categoryName") var categoryName: String
) {
    companion object {
        const val TABLE_NAME = "categoryEntity"
    }
}
