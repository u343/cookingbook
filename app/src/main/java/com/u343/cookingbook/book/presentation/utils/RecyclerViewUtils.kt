package com.u343.cookingbook.book.presentation.utils

import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

/**
 * Связывает кнопку и [RecyclerView], при прокрутке списка вниз кнопка будет скрываться,
 * при прокрутке вверх, будет показываться
 *
 * @param fab Кнопка с которой необходимо связять [RecyclerView]
 */
fun RecyclerView.attachFab(fab: FloatingActionButton) {
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (dy > 0)
                fab.hide()
            else if (dy < 0)
                fab.show()
        }
    })
}
