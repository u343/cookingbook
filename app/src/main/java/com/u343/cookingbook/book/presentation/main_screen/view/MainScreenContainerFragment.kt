package com.u343.cookingbook.book.presentation.main_screen.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.u343.cookingbook.R
import com.u343.cookingbook.book.domain.application.App
import com.u343.cookingbook.book.domain.interactors.MainScreenInteractor
import com.u343.cookingbook.book.models.recipe_db.CategoryEntity
import com.u343.cookingbook.book.presentation.main_screen.adapter.MainScreenViewPagerAdapter
import com.u343.cookingbook.book.presentation.main_screen.dialog.MainScreenBottomSheetClickListener
import com.u343.cookingbook.book.presentation.main_screen.dialog.MainScreenBottomSheetClickType
import com.u343.cookingbook.book.presentation.main_screen.dialog.MainScreenBottomSheetDialog
import com.u343.cookingbook.book.presentation.main_screen.viewmodel.MainScreenContainerViewModel
import com.u343.cookingbook.book.presentation.main_screen.viewmodel.MainScreenContainerViewModelFactory
import com.u343.cookingbook.book.presentation.router.CookingBookRouter
import java.lang.ref.WeakReference
import javax.inject.Inject

class MainScreenContainerFragment : Fragment(), MainScreenBottomSheetClickListener {

    @Inject
    lateinit var interactor: MainScreenInteractor

    private lateinit var viewPagerAdapter: MainScreenViewPagerAdapter
    private lateinit var router: WeakReference<CookingBookRouter>
    private lateinit var bottomSheetDialog: MainScreenBottomSheetDialog
    private lateinit var mainScreenContainerViewModel: MainScreenContainerViewModel

    private lateinit var viewPager: ViewPager2
    private lateinit var toggleButton: SwitchCompat
    lateinit var addButton: FloatingActionButton

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val activity = requireActivity()
        if (activity is CookingBookRouter) {
            router = WeakReference(activity)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main_screen_container, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        App.appComponent.inject(fragment = this@MainScreenContainerFragment)

        mainScreenContainerViewModel = ViewModelProvider(
            this, MainScreenContainerViewModelFactory(
                interactor
            )
        ).get(MainScreenContainerViewModel::class.java)

        initViews(view)
        setViewPagerListener()
        toggleButton.setOnClickListener { changeViewPagerFragment() }
        addButton.setOnClickListener { openCreationItemDialog() }
    }

    override fun bottomSheetItemClick(itemType: MainScreenBottomSheetClickType) {
        when (itemType) {
            MainScreenBottomSheetClickType.RECIPE -> {
                showCreationRecipeScreen()
            }
            MainScreenBottomSheetClickType.CATEGORY -> {
                showCreationCategoryDialog()
            }
        }
        bottomSheetDialog.dismiss()
    }

    private fun initViews(view: View) {
        viewPager = view.findViewById(R.id.main_screen_view_pager)
        toggleButton = view.findViewById(R.id.main_bottom_toggle_button)
        addButton = view.findViewById(R.id.main_screen_add_button)

        bottomSheetDialog = MainScreenBottomSheetDialog()

        viewPagerAdapter = MainScreenViewPagerAdapter(this)
        viewPager.adapter = viewPagerAdapter
    }

    private fun changeViewPagerFragment() {
        if (toggleButton.isChecked && viewPager.currentItem == 0) {
            viewPager.currentItem = viewPager.currentItem + 1
        } else if (viewPager.currentItem == 1) {
            viewPager.currentItem = viewPager.currentItem - 1
        }
    }

    private fun setViewPagerListener() {
        viewPager.registerOnPageChangeCallback(
            object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    if (position == 0) {
                        toggleButton.isChecked = false
                    } else if (position == 1) {
                        toggleButton.isChecked = true
                    }
                    super.onPageSelected(position)
                }
            }
        )
    }

    private fun showCreationRecipeScreen() {
        router.get()?.goToRecipeCreationScreen(0)
    }

    private fun showCreationCategoryDialog() {
        val inputTextField = EditText(requireActivity())
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(resources.getString(R.string.category_dialog_title))
            .setMessage(resources.getString(R.string.category_dialog_message))
            .setView(inputTextField)
            .setNegativeButton(resources.getString(R.string.cancel), null)
            .setPositiveButton(resources.getString(R.string.create)) { dialog, id ->
                addCategoryToBd(inputTextField.text.toString())
            }
            .show()
    }

    private fun addCategoryToBd(categoryName: String) {
        if (categoryName.isNotEmpty()) {
            mainScreenContainerViewModel.addNewCategory(
                CategoryEntity(categoryName = categoryName)
            )
        }
    }

    private fun openCreationItemDialog() {
        bottomSheetDialog.show(childFragmentManager, MainScreenBottomSheetDialog.TAG)
    }

    companion object {
        const val TAG = "MainScreenContainerFragment"

        /**
         * Фабричный метод для получению фрагмента
         */
        fun newInstance() = MainScreenContainerFragment()
    }
}
