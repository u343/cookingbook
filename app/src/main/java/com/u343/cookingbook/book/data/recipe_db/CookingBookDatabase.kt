package com.u343.cookingbook.book.data.recipe_db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.u343.cookingbook.book.data.recipe_db.dao.DaoCategory
import com.u343.cookingbook.book.data.recipe_db.dao.DaoCategoryRecipeCrossRef
import com.u343.cookingbook.book.data.recipe_db.dao.DaoRecipe
import com.u343.cookingbook.book.models.recipe_db.CategoryEntity
import com.u343.cookingbook.book.models.recipe_db.CategoryRecipeCrossRef
import com.u343.cookingbook.book.models.recipe_db.RecipeDataEntity

/**
 * [RoomDatabase] для работы с рецептами и их категориями
 */
@Database(entities = [CategoryEntity::class, RecipeDataEntity::class, CategoryRecipeCrossRef::class], version = 1)
abstract class CookingBookDatabase : RoomDatabase() {
    abstract fun recipeCategoryDao(): DaoCategory

    abstract fun recipesDao(): DaoRecipe

    abstract fun categoryRecipeCrossRefDao(): DaoCategoryRecipeCrossRef

    companion object {
        const val DATABASE_NAME = "recipes_room_database"
    }
}
