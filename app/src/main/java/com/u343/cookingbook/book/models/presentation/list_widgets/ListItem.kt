package com.u343.cookingbook.book.models.presentation.list_widgets

/**
 * Интерфейс который должны наследовать все дата классы, которые будут использоваться для работы
 * вместе с list widgets (виджеты предназначенные для отображения внутри recycler view)
 */
interface ListItem {
}