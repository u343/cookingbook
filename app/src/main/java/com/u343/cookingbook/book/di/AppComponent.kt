package com.u343.cookingbook.book.di

import com.u343.cookingbook.book.presentation.list_item_screen.ListItemsFragment
import com.u343.cookingbook.book.presentation.main_screen.view.MainScreenContainerFragment
import com.u343.cookingbook.book.presentation.main_screen.view.CategoriesAndRecipesFragment
import com.u343.cookingbook.book.presentation.main_screen.view.FavoritesRecipesFragment
import com.u343.cookingbook.book.presentation.recipes.RecipesListFragment
import com.u343.cookingbook.book.presentation.recipes.creation.CreationRecipeFragment
import com.u343.cookingbook.book.presentation.recipes.dialog.ChangeCategoryDialog
import com.u343.cookingbook.book.presentation.recipes.info.InfoRecipeFragment
import dagger.Component
import javax.inject.Singleton

@Component(modules = [CookingBookModule::class])
@Singleton
interface AppComponent {

    //Fragments
    fun inject(fragment: RecipesListFragment)
    fun inject(fragment: CreationRecipeFragment)
    fun inject(fragment: InfoRecipeFragment)
    fun inject(fragment: CategoriesAndRecipesFragment)
    fun inject(fragment: FavoritesRecipesFragment)
    fun inject(fragment: MainScreenContainerFragment)
    fun inject(fragment: ListItemsFragment)

    //Dialogs
    fun inject(dialogFragment: ChangeCategoryDialog)
}
