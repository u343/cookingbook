package com.u343.cookingbook.book.presentation.recipes.info

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.u343.cookingbook.book.domain.repositories.CategoryRepository
import com.u343.cookingbook.book.domain.repositories.RecipeRepository
import com.u343.cookingbook.book.models.recipe_db.CategoryEntity
import com.u343.cookingbook.book.models.recipe_db.RecipeDataEntity
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class InfoRecipeViewModel(
    private val recipeRepository: RecipeRepository,
    private val categoryRepository: CategoryRepository
) : ViewModel() {

    /**
     * [LiveData] с рецептом, который необходимо отобразить
     */
    private val _currentRecipe = MutableLiveData<RecipeDataEntity>()
    val currentRecipe: LiveData<RecipeDataEntity>
        get() = _currentRecipe

    /**
     * [LiveData] со списком категорий
     */
    private val _categoryList = MutableLiveData<List<CategoryEntity>>()
    val categoryList: LiveData<List<CategoryEntity>>
        get() = _categoryList

    fun loadAllCategories() {
        runBlocking {
            launch {
                _categoryList.value = categoryRepository.getAllCategories()
            }
        }
    }

    fun getRecipeById(id: Int) {
        runBlocking {
            launch {
                _currentRecipe.value = recipeRepository.getRecipeById(id)
            }
        }
    }

    fun deleteRecipe(recipe: RecipeDataEntity) {
        runBlocking {
            launch {
                recipeRepository.deleteRecipe(recipe)
            }
        }
    }

    fun updateRecipe(recipe: RecipeDataEntity) {
        runBlocking {
            launch {
                recipeRepository.updateRecipe(recipe)
            }
        }
    }

    fun setRecipeFavoriteStatus(id: Int, isFavorite: Boolean) {
        runBlocking {
            launch {
                val recipeData = recipeRepository.getRecipeById(id)

                recipeData.isFavorite = isFavorite
                recipeRepository.updateRecipe(recipeData)
            }
        }
    }
}
