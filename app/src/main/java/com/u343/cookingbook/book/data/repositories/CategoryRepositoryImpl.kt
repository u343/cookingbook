package com.u343.cookingbook.book.data.repositories

import com.u343.cookingbook.book.data.recipe_db.dao.DaoCategory
import com.u343.cookingbook.book.data.recipe_db.dao.DaoCategoryRecipeCrossRef
import com.u343.cookingbook.book.domain.repositories.CategoryRepository
import com.u343.cookingbook.book.models.recipe_db.CategoryEntity
import com.u343.cookingbook.book.models.recipe_db.CategoryRecipeCrossRef

/**
 * Реализация репозитория для взаимодействия с БД категорий
 *
 * @property categoryDao ДАО для взаимодействия с бд категорий
 */
class CategoryRepositoryImpl(
    private val categoryDao: DaoCategory
) : CategoryRepository {

    override suspend fun getAllCategories(): List<CategoryEntity> = categoryDao.getAll().reversed()

    override suspend fun addNewCategory(categoryData: CategoryEntity) {
        categoryDao.insert(categoryData)
    }

    override suspend fun deleteCategory(categoryData: CategoryEntity) {
        categoryDao.delete(categoryData)
    }

    override suspend fun updateCategory(categoryData: CategoryEntity) {
        categoryDao.update(categoryData)
    }

    override suspend fun getCategoryById(idCategory: Int): CategoryEntity = categoryDao.getCategoryById(idCategory)
}
