package com.u343.cookingbook.book.presentation.recipes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.u343.cookingbook.book.domain.repositories.CategoryRepository
import com.u343.cookingbook.book.domain.repositories.RecipeRepository
import com.u343.cookingbook.book.models.recipe_db.CategoryEntity
import com.u343.cookingbook.book.models.recipe_db.RecipeDataEntity
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class RecipesListViewModel(
    private val recipeRepository: RecipeRepository,
    private val categoryRepository: CategoryRepository
) : ViewModel() {

    /**
     * [LiveData] со списком рецептов
     */
    private val _recipesList = MutableLiveData<List<RecipeDataEntity>>()
    val recipesList: LiveData<List<RecipeDataEntity>>
        get() = _recipesList

    private val _categoryData = MutableLiveData<CategoryEntity>()
    val categoryData: LiveData<CategoryEntity>
        get() = _categoryData

    fun loadAllRecipesByCategory(idCategory: Int) {
        runBlocking {
            launch {
                _recipesList.value = recipeRepository.getAllRecipesByCategory(idCategory)
            }
        }
    }

    fun getCategoryById(idCategory: Int) {
        runBlocking {
            launch {
                _categoryData.value = categoryRepository.getCategoryById(idCategory)
            }
        }
    }
}
