package com.u343.cookingbook.book.presentation.widgets.list_widgets

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import com.u343.cookingbook.R
import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.RecipeListItem
import com.u343.cookingbook.book.presentation.widgets.BaseViewHolder

/**
 * Кликабельный виджет рецепта
 *
 * @param action действие, которое произойдет при клике на виджет
 */
class RecipeListWidget(private val action: RecipeListWidgetAction) : ListWidget<View, RecipeListItem> {

    override fun isRelativeItem(item: ListItem): Boolean = item is RecipeListItem

    override fun getLayoutId(): Int = R.layout.list_item_recipe

    override fun getViewHolder(
        layoutInflater: LayoutInflater,
        parent: ViewGroup
    ): BaseViewHolder<View, RecipeListItem> {

        val view = layoutInflater
            .inflate(R.layout.list_item_recipe, parent, false)

        return RecipeViewHolder(action, view)
    }

    override fun getDiffUtil(): DiffUtil.ItemCallback<RecipeListItem> = diffUtil

    private val diffUtil = object : DiffUtil.ItemCallback<RecipeListItem>() {
        override fun areItemsTheSame(oldItem: RecipeListItem, newItem: RecipeListItem) =
            oldItem.recipeId == newItem.recipeId

        override fun areContentsTheSame(oldItem: RecipeListItem, newItem: RecipeListItem) = oldItem == newItem
    }
}

/**
 * ViewHolder для карточки рецепта
 *
 * @param action действие, которое произойдет при клике на виджет
 */
class RecipeViewHolder(
    private val action: RecipeListWidgetAction,
    view: View
) : BaseViewHolder<View, RecipeListItem>(view) {

    private val nameView: TextView = view.findViewById(R.id.list_item_recipe_name)
    private val valueView: TextView = view.findViewById(R.id.list_item_recipe_value)
    private val favoriteIcon: ImageView = view.findViewById(R.id.list_item_recipe_favorite_icon)

    override fun onBind(item: RecipeListItem) {
        item.run {
            nameView.text = recipeName
            valueView.text = "valueView" //TODO добавить список категорий
            setFavoriteStatusIcon(isFavorite)
        }

        itemView.setOnClickListener { action.recipeListWidgetClick(item) }
    }

    private fun setFavoriteStatusIcon(isFavorite: Boolean) {
        favoriteIcon.visibility = if (isFavorite) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
}

/**
 * Интерфейс, с методом обработки клика на интерфейс
 * Необходимо реализовать каждому экрану, который собирается использовать этот виджет
 */
interface RecipeListWidgetAction {

    fun recipeListWidgetClick(item: RecipeListItem)
}
