package com.u343.cookingbook.book.presentation.recipes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.u343.cookingbook.R
import com.u343.cookingbook.book.models.recipe_db.RecipeDataEntity

class RecipesListAdapter(
    private val recipeItemOnClickListener: RecipeItemOnClickListener
) : RecyclerView.Adapter<RecipesListAdapter.ViewHolder>() {

    private var recipesList: List<RecipeDataEntity> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.recipes_item, parent, false)

        return ViewHolder(view, recipeItemOnClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(recipesList[position])
    }

    override fun getItemCount(): Int = recipesList.size

    /**
     * Установка списка рецептов
     *
     * @param data список рецептов
     */
    fun setData(data: List<RecipeDataEntity>) {
        recipesList = data
        notifyDataSetChanged()
    }

    class ViewHolder(
        view: View,
        private val recipeItemOnClickListener: RecipeItemOnClickListener
    ) : RecyclerView.ViewHolder(view) {

        private val titleTextView: TextView = itemView.findViewById(R.id.recipe_item_title)
        private val descriptionTextView: TextView = itemView.findViewById(R.id.recipe_item_description)

        fun onBind(data: RecipeDataEntity) {
            titleTextView.text = data.recipeName
            descriptionTextView.text = data.description
            itemView.setOnClickListener { recipeItemOnClickListener.onItemClick(data.recipeId) }
        }
    }

    fun interface RecipeItemOnClickListener {

        fun onItemClick(idRecipe: Int)
    }
}
