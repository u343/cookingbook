package com.u343.cookingbook.book.presentation.widgets.list_widgets

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import com.u343.cookingbook.R
import com.u343.cookingbook.book.models.presentation.list_widgets.CategoryListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem
import com.u343.cookingbook.book.presentation.widgets.BaseViewHolder

/**
 * Кликабельный виджет категории
 *
 * @param action действие, которое произойдет при клике на виджет
 */
class CategoryListWidget(private val action: CategoryListWidgetAction) : ListWidget<View, CategoryListItem> {

    override fun isRelativeItem(item: ListItem): Boolean = item is CategoryListItem

    override fun getLayoutId(): Int = R.layout.list_item_category

    override fun getViewHolder(
        layoutInflater: LayoutInflater,
        parent: ViewGroup
    ): BaseViewHolder<View, CategoryListItem> {

        val view = layoutInflater
            .inflate(R.layout.list_item_category, parent, false)

        return CategoryViewHolder(action, view)
    }

    override fun getDiffUtil(): DiffUtil.ItemCallback<CategoryListItem> = diffUtil

    private val diffUtil = object : DiffUtil.ItemCallback<CategoryListItem>() {
        override fun areItemsTheSame(oldItem: CategoryListItem, newItem: CategoryListItem) =
            oldItem.categoryId == newItem.categoryId

        override fun areContentsTheSame(oldItem: CategoryListItem, newItem: CategoryListItem) = oldItem == newItem
    }
}

/**
 * ViewHolder для карточки категории
 *
 * @param action действие, которое произойдет при клике на виджет
 */
class CategoryViewHolder(
    private val action: CategoryListWidgetAction,
    view: View
) : BaseViewHolder<View, CategoryListItem>(view) {

    private val nameView: TextView = view.findViewById(R.id.list_item_category_name)
    private val valueView: TextView = view.findViewById(R.id.list_item_category_value)

    override fun onBind(item: CategoryListItem) {
        item.run {
            nameView.text = categoryName
            valueView.text = recipeCount
        }
        itemView.setOnClickListener { action.categoryListWidgetClick(item) }
    }
}

/**
 * Интерфейс, с методом обработки клика на интерфейс
 * Необходимо реализовать каждому экрану, который собирается использовать этот виджет
 */
interface CategoryListWidgetAction {

    fun categoryListWidgetClick(item: CategoryListItem)
}
