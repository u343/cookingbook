package com.u343.cookingbook.book.domain.repositories

import com.u343.cookingbook.book.models.recipe_db.CategoryEntity

/**
 * Репозиторий для взаимодействия с БД категорий рецептов
 */
interface CategoryRepository {

    /**
     * Получить все категории рецептов
     *
     * @return список моделей категорий
     */
    suspend fun getAllCategories(): List<CategoryEntity>

    /**
     * Добавить новую категорию
     *
     * @param categoryData модель категории, которую нужно добавить
     */
    suspend fun addNewCategory(categoryData: CategoryEntity)

    /**
     * Удалить указанную категорию
     *
     * @param categoryData модель категории, которую нужно удалить
     */
    suspend fun deleteCategory(categoryData: CategoryEntity)

    /**
     * Изменить указанную категорию
     *
     * @param categoryData модель категории, которую нужно изменить
     */
    suspend fun updateCategory(categoryData: CategoryEntity)

    /**
     * Получить категорию по ее id
     *
     * @param idCategory идентификатор категории
     */
    suspend fun getCategoryById(idCategory: Int): CategoryEntity
}
