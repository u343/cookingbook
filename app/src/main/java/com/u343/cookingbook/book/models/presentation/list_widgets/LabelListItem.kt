package com.u343.cookingbook.book.models.presentation.list_widgets

/**
 * Data class виджета заголовка
 *
 * @property title текст заголовка
 * @property value доплниетельное значение, которое нужно отобразить в заголовке
 */
data class LabelListItem(
    val title: String,
    val value: String?
) : ListItem