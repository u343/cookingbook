package com.u343.cookingbook.book.presentation.list_item_screen

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.u343.cookingbook.R
import com.u343.cookingbook.book.domain.application.App
import com.u343.cookingbook.book.domain.repositories.CategoryRepository
import com.u343.cookingbook.book.domain.repositories.RecipeRepository
import com.u343.cookingbook.book.models.presentation.list_item_screen.ListItemScreenType
import com.u343.cookingbook.book.models.presentation.list_widgets.CategoryListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.RecipeListItem
import com.u343.cookingbook.book.presentation.widgets.ListWidgetsRecyclerViewAdapter
import com.u343.cookingbook.book.presentation.router.CookingBookRouter
import com.u343.cookingbook.book.presentation.widgets.list_widgets.CategoryListWidget
import com.u343.cookingbook.book.presentation.widgets.list_widgets.CategoryListWidgetAction
import com.u343.cookingbook.book.presentation.widgets.list_widgets.ListWidget
import com.u343.cookingbook.book.presentation.widgets.list_widgets.RecipeListWidget
import com.u343.cookingbook.book.presentation.widgets.list_widgets.RecipeListWidgetAction
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Фрагмент, который может отображать либо спискок категорий, либо список рецептов.
 * Может быть расширен и отображать список других элементов
 */
class ListItemsFragment : Fragment(), CategoryListWidgetAction, RecipeListWidgetAction {

    @Inject
    lateinit var recipesRepository: RecipeRepository

    @Inject
    lateinit var categoryRepository: CategoryRepository

    private lateinit var listItemsRecyclerView: RecyclerView
    private lateinit var addButton: FloatingActionButton

    private lateinit var listItemsRecyclerViewAdapter: ListWidgetsRecyclerViewAdapter
    private lateinit var listItemsViewModel: ListItemsViewModel
    private lateinit var router: WeakReference<CookingBookRouter>

    private var screenWorkType: ListItemScreenType? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val activity = requireActivity()
        if (activity is CookingBookRouter) {
            router = WeakReference(activity)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list_items, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        App.appComponent.inject(fragment = this@ListItemsFragment)

        listItemsViewModel = ViewModelProvider(
            this, ListItemsViewModelFactory(
                categoryRepository,
                recipesRepository
            )
        ).get(ListItemsViewModel::class.java)

        setScreenWorkType()
        initView(view)
        initObserves()
        loadScreenContent()
    }

    override fun categoryListWidgetClick(item: CategoryListItem) {
        router.get()?.goToRecipeInfoScreen(item.categoryId)
    }

    override fun recipeListWidgetClick(item: RecipeListItem) {
        router.get()?.goToRecipeInfoScreen(item.recipeId)
    }

    private fun setScreenWorkType() {
        arguments?.getSerializable(ARG_SCREEN_TYPE_KEY)?.let { type ->
            screenWorkType = type as ListItemScreenType
        }
    }

    private fun initView(view: View) {
        listItemsRecyclerView = view.findViewById(R.id.list_items_recycler_view)
        addButton = view.findViewById(R.id.list_items_add_button)

        listItemsRecyclerViewAdapter = ListWidgetsRecyclerViewAdapter(getWidgets())
        listItemsRecyclerView.adapter = listItemsRecyclerViewAdapter
    }

    private fun initObserves() {
        listItemsViewModel.recyclerViewData.observe(viewLifecycleOwner) { data ->
            setDataToRecyclerView(data)
        }
    }

    private fun loadScreenContent() {
        when (screenWorkType) {
            ListItemScreenType.ALL_CATEGORIES -> listItemsViewModel.loadCategories()
            ListItemScreenType.CATEGORY_CONTENT -> {
                arguments?.getInt(ARG_ID_CATEGORY_KEY)?.let { id ->
                    listItemsViewModel.loadRecipesForCategory(id)
                }
            }
        }
    }

    private fun setDataToRecyclerView(data: List<ListItem>) {
        listItemsRecyclerViewAdapter.submitList(data)
    }

    /**
     * Виджеты которые бутут отображены в recyclerView
     */
    private fun getWidgets(): List<ListWidget<*, *>> {
        return when (screenWorkType) {
            ListItemScreenType.CATEGORY_CONTENT -> {
                listOf(CategoryListWidget(this))
            }
            else -> listOf(RecipeListWidget(this))
        }
    }

    companion object {

        const val TAG = "ListItemsFragment"
        private const val ARG_SCREEN_TYPE_KEY = "typeScreenExtra"
        private const val ARG_ID_CATEGORY_KEY = "idCategoryExtra"

        /**
         * Фабричный метод для получению фрагмента
         *
         * При воборе типа [ListItemScreenType.CATEGORY_CONTENT] необходимо передать также id категории
         *
         * @param screenType Режим в котором будет работать экран
         * @param idCategory id категории
         */
        fun newInstance(screenType: ListItemScreenType, idCategory: Int? = null) = ListItemsFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARG_SCREEN_TYPE_KEY, screenType)
                idCategory?.let { putInt(ARG_ID_CATEGORY_KEY, idCategory) }
            }
        }
    }
}
