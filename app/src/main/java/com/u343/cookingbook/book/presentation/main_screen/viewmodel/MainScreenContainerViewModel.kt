package com.u343.cookingbook.book.presentation.main_screen.viewmodel

import androidx.lifecycle.ViewModel
import com.u343.cookingbook.book.domain.interactors.MainScreenInteractor
import com.u343.cookingbook.book.models.recipe_db.CategoryEntity
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/**
 * [ViewModel] контейнера главного экрана
 *
 * @param interactor Интерактор главного экрана
 */
class MainScreenContainerViewModel(
    private val interactor: MainScreenInteractor
) : ViewModel() {

    fun addNewCategory(category: CategoryEntity) {
        runBlocking {
            launch {
                interactor.addNewCategory(category)
            }
        }
    }
}
