package com.u343.cookingbook.book.presentation.recipes.creation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.u343.cookingbook.book.domain.repositories.RecipeRepository
import com.u343.cookingbook.book.models.recipe_db.RecipeDataEntity
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/**
 * [ViewModel] для экрана создания/редактирования рецепта
 */
class CreationRecipeViewModel(
    private val recipeRepository: RecipeRepository
) : ViewModel() {

    /**
     * [LiveData] с рецептом, который необходимо имзменить
     */
    private val _currentRecipe = MutableLiveData<RecipeDataEntity>()
    val currentRecipe: LiveData<RecipeDataEntity>
        get() = _currentRecipe

    fun getRecipeById(id: Int) {
        runBlocking {
            launch {
                _currentRecipe.value = recipeRepository.getRecipeById(id)
            }
        }
    }

    fun setRecipe(recipe: RecipeDataEntity) {
        runBlocking {
            launch {
                recipeRepository.addNewRecipe(recipe)
            }
        }
    }

    fun updateRecipe(recipe: RecipeDataEntity) {
        runBlocking {
            launch {
                recipeRepository.updateRecipe(recipe)
            }
        }
    }
}
