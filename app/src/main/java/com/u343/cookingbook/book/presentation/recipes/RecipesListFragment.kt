package com.u343.cookingbook.book.presentation.recipes

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.u343.cookingbook.R
import com.u343.cookingbook.book.domain.application.App
import com.u343.cookingbook.book.domain.repositories.CategoryRepository
import com.u343.cookingbook.book.domain.repositories.RecipeRepository
import com.u343.cookingbook.book.models.recipe_db.RecipeDataEntity
import com.u343.cookingbook.book.presentation.router.CookingBookRouter
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Фрагмент с рецептами
 */
class RecipesListFragment : Fragment() {

    @Inject
    lateinit var recipesRepository: RecipeRepository

    @Inject
    lateinit var categoryRepository: CategoryRepository

    private lateinit var recipesRecyclerView: RecyclerView
    private lateinit var addButton: FloatingActionButton

    private lateinit var recipesViewModel: RecipesListViewModel
    private lateinit var router: WeakReference<CookingBookRouter>

    private var recipesRecyclerViewAdapter: RecipesListAdapter? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val activity = requireActivity()
        if (activity is CookingBookRouter) {
            router = WeakReference(activity)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recipes_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        App.appComponent.inject(fragment = this@RecipesListFragment)


        recipesViewModel = RecipesListViewModel(recipesRepository, categoryRepository)
        // TODO поправить получение ViewModel

        initViews(view)
        initObservers()
        setUpToolbar()

        arguments?.getInt(ARG_KEY)?.let { idCategory ->
            recipesViewModel.loadAllRecipesByCategory(idCategory)
            recipesViewModel.getCategoryById(idCategory)
            addButton.setOnClickListener { router.get()?.goToRecipeCreationScreen(idCategory) }
        } ?: run { //TODO сделать алерт об ошибке
        }
    }

    override fun onStop() {
        super.onStop()

        changeToolbarTittle(resources.getString(R.string.app_name))
    }

    override fun onDestroy() {
        super.onDestroy()

        recipesRecyclerViewAdapter = null
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            requireActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initViews(view: View) {
        recipesRecyclerView = view.findViewById(R.id.recipes_recycle_view)
        addButton = view.findViewById(R.id.recipes_add_button)

        recipesRecyclerViewAdapter = RecipesListAdapter(this::onItemClick)
        recipesRecyclerView.adapter = recipesRecyclerViewAdapter
    }

    private fun initObservers() {
        recipesViewModel.recipesList.observe(viewLifecycleOwner) { data ->
            setDataToRecyclerView(data)
        }
        recipesViewModel.categoryData.observe(viewLifecycleOwner) { category ->
            changeToolbarTittle(category.categoryName)
        }
    }

    private fun setUpToolbar() {
        setHasOptionsMenu(true)
    }

    private fun changeToolbarTittle(titleText: String) {
        requireActivity().title = titleText
    }

    private fun setDataToRecyclerView(data: List<RecipeDataEntity>) {
        recipesRecyclerViewAdapter?.setData(data)
    }

    private fun onItemClick(idRecipe: Int) {
        router.get()?.goToRecipeInfoScreen(idRecipe)
    }

    companion object {
        const val TAG = "RecipesFragment"
        private const val ARG_KEY = "id_category_extra"

        /**
         * Фабричный метод для получению фрагмента
         *
         * @param idCategory идентификатор категории, к которой относится рецепт
         */
        fun newInstance(idCategory: Int) = RecipesListFragment().apply {
            arguments = Bundle().apply {
                putInt(ARG_KEY, idCategory)
            }
        }
    }
}
