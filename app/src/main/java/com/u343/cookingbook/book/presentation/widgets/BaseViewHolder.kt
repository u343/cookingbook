package com.u343.cookingbook.book.presentation.widgets

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem

/**
 * Базовый ViewHolder для виджетов
 */
abstract class BaseViewHolder<out V : View, in I : ListItem>(
    val view: V
) : RecyclerView.ViewHolder(view) {

    abstract fun onBind(item: I)
}
