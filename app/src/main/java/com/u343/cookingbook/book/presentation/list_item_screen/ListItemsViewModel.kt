package com.u343.cookingbook.book.presentation.list_item_screen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.u343.cookingbook.book.domain.repositories.CategoryRepository
import com.u343.cookingbook.book.domain.repositories.RecipeRepository
import com.u343.cookingbook.book.models.presentation.list_widgets.CategoryListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.RecipeListItem
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/**
 * [ViewModel] для экрана со списком всех рецептов/категорий
 */
class ListItemsViewModel(
    private val categoryRepository: CategoryRepository,
    private val recipeRepository: RecipeRepository
) : ViewModel() {

    /**
     * [LiveData] со списком категорий
     */
    private val _recyclerViewData = MutableLiveData<List<ListItem>>()
    val recyclerViewData: LiveData<List<ListItem>>
        get() = _recyclerViewData

    fun loadCategories() {
        runBlocking {
            launch {
                createCategoriesViewData()
            }
        }
    }

    fun loadRecipesForCategory(categoryId: Int) {
        runBlocking {
            launch {
                createRecipesViewData(categoryId)
            }
        }
    }

    private suspend fun createCategoriesViewData() {

        _recyclerViewData.value = categoryRepository.getAllCategories().map { categoryData ->
            CategoryListItem(
                categoryId = categoryData.categoryId,
                categoryName = categoryData.categoryName,
                image = null,
                recipeCount = "0" // TODO избежать 2х запросов к БД, сделать отдельный дата класс и за один раз получать всю информацию
            )
        }
    }

    private suspend fun createRecipesViewData(categoryId: Int) {

        _recyclerViewData.value = recipeRepository.getAllRecipesByCategory(categoryId).map { recipeData ->
            RecipeListItem(
                recipeId = recipeData.recipeId,
                recipeName = recipeData.recipeName,
                image = null,
                isFavorite = recipeData.isFavorite
            )
        }
    }
}
