package com.u343.cookingbook.book.models.recipe_db

import androidx.room.Entity

/**
 * Модель для реализации связи many-to-many между категориями и рецептами
 *
 * В таблице хранятся ссылки на категорию и рецепт, которые между связаны.
 * В категории может хранится несколько рецептов, один рецепт может находиться
 * в нескольких категориях
 *
 * @property categoryId id категрии
 * @property recipeId id рецепта
 */
@Entity(primaryKeys = ["categoryId", "recipeId"])
data class CategoryRecipeCrossRef(
    val categoryId: Int,
    val recipeId: Int
)
