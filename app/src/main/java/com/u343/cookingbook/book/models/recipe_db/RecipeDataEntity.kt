package com.u343.cookingbook.book.models.recipe_db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Модель с данными рецептов
 *
 * @property recipeId id рецепта, параметр проставляется автоматически, не нужно его явно передавать
 * @property recipeName название рецепта
 * @property description краткое описание рецепта
 * @property ingredients ингредиента для рецепта
 * @property recipe рецепт блюда
 * @property isFavorite является ли рецепт любимым
 */
@Entity
data class RecipeDataEntity(
    @PrimaryKey(autoGenerate = true) var recipeId: Int = 0,
    @ColumnInfo(name = "recipe_name") var recipeName: String,
    @ColumnInfo(name = "description") var description: String,
    @ColumnInfo(name = "ingredients") var ingredients: String,
    @ColumnInfo(name = "recipe_text") var recipe: String,
    @ColumnInfo(name = "favorite_status") var isFavorite: Boolean = false
) {
    companion object {
        const val TABLE_NAME = "recipeDataEntity"
    }
}
