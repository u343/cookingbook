package com.u343.cookingbook.book.presentation.main_screen.dialog

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.u343.cookingbook.R

/**
 * Шторка главного экрана
 *
 * Используется для создания элементов (рецептов или категорий)
 */
class MainScreenBottomSheetDialog : BottomSheetDialogFragment() {

    private var listener: MainScreenBottomSheetClickListener? = null

    private lateinit var addCategoryButton: Button
    private lateinit var addRecipeButton: Button

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val parent = requireParentFragment()
        if (parent is MainScreenBottomSheetClickListener) {
            listener = parent
        } else {
            throw RuntimeException(
                "$context must implement MainScreenBottomSheetClickListener interface"
            )
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_bottom_sheet_main_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView(view)
    }

    private fun initView(view: View) {
        addCategoryButton = view.findViewById(R.id.bottom_sheet_create_category_button)
        addRecipeButton = view.findViewById(R.id.bottom_sheet_create_recipe_button)

        addCategoryButton.setOnClickListener { listener?.bottomSheetItemClick(MainScreenBottomSheetClickType.CATEGORY) }
        addRecipeButton.setOnClickListener { listener?.bottomSheetItemClick(MainScreenBottomSheetClickType.RECIPE) }
    }

    companion object {
        const val TAG = "MainScreenBottomSheetDialog"

        /**
         * Фабричный метод для получению фрагмента
         */
        fun newInstance() = MainScreenBottomSheetDialog()
    }
}
