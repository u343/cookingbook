package com.u343.cookingbook.book.data.recipe_db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.u343.cookingbook.book.models.recipe_db.CategoryRecipeCrossRef

@Dao
interface DaoCategoryRecipeCrossRef {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: CategoryRecipeCrossRef)

    @Delete
    suspend fun delete(data: CategoryRecipeCrossRef)
}
