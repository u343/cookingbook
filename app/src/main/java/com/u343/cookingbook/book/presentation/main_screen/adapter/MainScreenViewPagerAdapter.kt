package com.u343.cookingbook.book.presentation.main_screen.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.u343.cookingbook.book.presentation.main_screen.view.CategoriesAndRecipesFragment
import com.u343.cookingbook.book.presentation.main_screen.view.FavoritesRecipesFragment

class MainScreenViewPagerAdapter(fm: Fragment) :
    FragmentStateAdapter(fm) {
    private val FIRST_PAGE_POSITION = 0
    private val SECOND_PAGE_POSITION = 1
    private val PAGES_COUNT = 2

    override fun getItemCount(): Int = PAGES_COUNT

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            FIRST_PAGE_POSITION -> CategoriesAndRecipesFragment.newInstance()
            SECOND_PAGE_POSITION -> FavoritesRecipesFragment.newInstance()
            else -> throw IllegalArgumentException("Error num $position")
        }
    }
}
