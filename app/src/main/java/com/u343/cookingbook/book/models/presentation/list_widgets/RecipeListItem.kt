package com.u343.cookingbook.book.models.presentation.list_widgets

import android.graphics.drawable.Drawable

/**
 * Data class виджета рецепта
 *
 * @property recipeId идентификатор рецепта
 * @property recipeName название рецепта
 * @property image изображение рецепта
 * @property isFavorite является ли рецепт любимым
 */
data class RecipeListItem(
    val recipeId: Int,
    val recipeName: String,
    val image: Drawable?,
    val isFavorite: Boolean
) : ListItem
