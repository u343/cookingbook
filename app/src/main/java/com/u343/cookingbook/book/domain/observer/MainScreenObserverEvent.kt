package com.u343.cookingbook.book.domain.observer

/**
 * Интерфейс, который должны реализовать классы, которые хотят подписаться на [MainScreenObserver]
 */
interface MainScreenObserverEvent {

    /**
     * Действие, совершаемое на сигнал об обновлении данных
     */
    fun event()
}