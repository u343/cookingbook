package com.u343.cookingbook.book.presentation.router

import com.u343.cookingbook.book.models.presentation.list_item_screen.ListItemScreenType
import com.u343.cookingbook.book.presentation.list_item_screen.ListItemsFragment

/**
 * Роутер для переходов между экранами приложения
 */
interface CookingBookRouter {

    /**
     * Переход к главному экрану РАЗРАБОТКА!!!
     */
    fun goToMainScreen()

    /**
     * Переход к экрану с рецептами
     *
     * @param idCategory идентификатор категории, которую нужно открыть
     */
    fun goToRecipesScreen(idCategory: Int)

    /**
     * Переход к экрану создания/редактирования рецепта
     *
     * @param idCategory идентификатор категории, к которой нужно добавить рецепт.
     *                   Если экран открывается для изменения рецепта, то
     *                   передовать id не нужно
     * @param idRecipe идентификатор рецепта который нужно изменить, передавать только в случае изменения рецепта
     */
    fun goToRecipeCreationScreen(idCategory: Int? = null, idRecipe: Int? = null)

    /**
     * Переход к экрану с подробной информацей о рецепте
     *
     * @param idRecipe идентификатор рецепта, который нужно показать
     */
    fun goToRecipeInfoScreen(idRecipe: Int)

    /**
     * Переход к экрану с камерой
     */
    fun goToCameraScreen()

    /**
     * Переход к экрану со всемми категориями
     */
    fun goToCategoriesScreen(screenType: ListItemScreenType, idCategory: Int? = null)
}
