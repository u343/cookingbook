package com.u343.cookingbook.book.models.presentation.list_widgets

import android.graphics.drawable.Drawable

/**
 * Data class виджета категории
 *
 * @property categoryId идентификатор категории
 * @property categoryName название категории
 * @property image изображение категории
 * @property recipeCount количество рецептов, принадлежащих категории
 */
data class CategoryListItem(
    val categoryId: Int,
    val categoryName: String,
    val image: Drawable?,
    val recipeCount: String
) : ListItem