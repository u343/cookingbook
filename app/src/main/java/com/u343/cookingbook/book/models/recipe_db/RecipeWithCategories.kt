package com.u343.cookingbook.book.models.recipe_db

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

/**
 * Модель рецептов с категорими, в которых эти рецепты находятся
 *
 * @property recipe модель рецепта
 * @property categories категории, связанные с данным рецептом
 */
data class RecipeWithCategories(
    @Embedded val recipe: RecipeDataEntity,
    @Relation(
        parentColumn = "recipeId",
        entityColumn = "categoryId",
        associateBy = Junction(CategoryRecipeCrossRef::class)
    )
    val categories: List<CategoryEntity>
)
