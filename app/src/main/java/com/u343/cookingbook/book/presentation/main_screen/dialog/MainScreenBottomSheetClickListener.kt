package com.u343.cookingbook.book.presentation.main_screen.dialog

/**
 * Обработчик нажатия на элемент шторки главного экрана
 */
interface MainScreenBottomSheetClickListener {

    /**
     * Событие нажатия на элемент
     *
     * @param itemType Тип элемента который был нажат
     */
    fun bottomSheetItemClick(itemType: MainScreenBottomSheetClickType)
}
