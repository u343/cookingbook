package com.u343.cookingbook.book.presentation.recipes.info

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.ToggleButton
import androidx.fragment.app.Fragment
import com.u343.cookingbook.R
import com.u343.cookingbook.book.domain.application.App
import com.u343.cookingbook.book.domain.repositories.CategoryRepository
import com.u343.cookingbook.book.domain.repositories.RecipeRepository
import com.u343.cookingbook.book.models.recipe_db.CategoryEntity
import com.u343.cookingbook.book.models.recipe_db.RecipeDataEntity
import com.u343.cookingbook.book.presentation.recipes.dialog.ChangeCategoryDialog
import com.u343.cookingbook.book.presentation.recipes.dialog.ChangeCategoryListener
import com.u343.cookingbook.book.presentation.router.CookingBookRouter
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Фрагмент на котором отображается подробная информация о рецепте
 *
 * Через этот фрагмент также можно удалять/изменять рецепт и также изменять категорию рецепта
 */
class InfoRecipeFragment : Fragment(), ChangeCategoryListener {

    @Inject
    lateinit var recipeRepository: RecipeRepository
    @Inject
    lateinit var categoryRepository: CategoryRepository

    private lateinit var infoRecipeViewModel: InfoRecipeViewModel
    private lateinit var recipeData: RecipeDataEntity
    private lateinit var router: WeakReference<CookingBookRouter>

    private lateinit var favoriteToggleButton: ToggleButton
    private lateinit var titleTextView: TextView
    private lateinit var descriptionTextView: TextView
    private lateinit var ingredientsTextView: TextView
    private lateinit var recipeTextView: TextView

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val activity = requireActivity()
        if (activity is CookingBookRouter) {
            router = WeakReference(activity)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_info_recipe, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        App.appComponent.inject(fragment = this@InfoRecipeFragment)

        infoRecipeViewModel = InfoRecipeViewModel(recipeRepository, categoryRepository)

        initViews(view)
        initObservers()
        setUpToolbar()

        val recipeId = arguments?.getInt(ARG_RECIPE_KEY)
        recipeId?.let {
            infoRecipeViewModel.getRecipeById(recipeId)
            favoriteToggleButton.setOnCheckedChangeListener { _, isChecked ->
                infoRecipeViewModel.setRecipeFavoriteStatus(recipeId, isChecked)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.info_recipe_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            requireActivity().onBackPressed()
            super.onOptionsItemSelected(item)
        }
        R.id.info_menu_delete -> {
            deleteRecipe(recipeData)
            true
        }
        R.id.info_menu_update -> {
            updateRecipe(recipeData.recipeId)
            true
        }
        R.id.info_menu_change_category -> {
            changeCategory()
            true
        }
        else -> false
    }

    override fun onChangeDialogItemClick(idCategory: Int) {
        /*recipeData.apply {
            categoryId = idCategory
        }
        infoRecipeViewModel.updateRecipe(recipeData)*/
    }

    private fun initViews(view: View) {
        titleTextView = view.findViewById(R.id.info_recipe_main_title)
        descriptionTextView = view.findViewById(R.id.info_recipe_description_content)
        ingredientsTextView = view.findViewById(R.id.info_recipe_ingredients_content)
        recipeTextView = view.findViewById(R.id.info_recipe_recipe_content)
        favoriteToggleButton = view.findViewById(R.id.list_item_recipe_favorite_button)
    }

    private fun initObservers() {
        infoRecipeViewModel.currentRecipe.observe(viewLifecycleOwner) { recipe ->
            showRecipe(recipe)
            setFavoriteToggleButtonStatus(recipe)
            recipeData = recipe
        }
        infoRecipeViewModel.categoryList.observe(viewLifecycleOwner) { categories ->
            startChangeCategoryDialog(categories)
        }
    }

    private fun setUpToolbar() {
        setHasOptionsMenu(true)
    }

    private fun showRecipe(recipeData: RecipeDataEntity) {
        recipeData.run {
            titleTextView.text = recipeName
            descriptionTextView.text = description
            ingredientsTextView.text = ingredients
            recipeTextView.text = recipe
        }
    }

    private fun deleteRecipe(recipeData: RecipeDataEntity) {
        infoRecipeViewModel.deleteRecipe(recipeData)
        closeThisFragment()
    }

    private fun updateRecipe(idRecipe: Int) {
        router.get()?.goToRecipeCreationScreen(null, idRecipe)
    }

    /**
     * Запускает цепочку событий, которая в конечном итоге отобразит диалог с выбором категории
     */
    private fun changeCategory() {
        infoRecipeViewModel.loadAllCategories()
    }

    private fun closeThisFragment() {
        requireActivity().supportFragmentManager.popBackStack()
    }

    private fun startChangeCategoryDialog(categoryList: List<CategoryEntity>) {
        ChangeCategoryDialog(categoryList).show(childFragmentManager, ChangeCategoryDialog.TAG)
    }

    private fun setFavoriteToggleButtonStatus(recipeData: RecipeDataEntity) {
        favoriteToggleButton.isChecked = recipeData.isFavorite
    }

    companion object {
        const val TAG = "InfoRecipeFragment"
        private const val ARG_RECIPE_KEY = "id_recipe_extra"

        /**
         * Фабричный метод для получению фрагмента
         *
         * @param idRecipe идентификатор рецепта, который нужно отобразить на этом экране
         */
        fun newInstance(idRecipe: Int) = InfoRecipeFragment().apply {
            arguments = Bundle().apply {
                putInt(ARG_RECIPE_KEY, idRecipe)
            }
        }
    }
}
