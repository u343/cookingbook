package com.u343.cookingbook.book.presentation.main_screen.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.u343.cookingbook.book.domain.interactors.MainScreenInteractor
import com.u343.cookingbook.book.domain.observer.MainScreenObserverEvent
import com.u343.cookingbook.book.models.presentation.list_widgets.CategoryListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.LabelListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.ListItem
import com.u343.cookingbook.book.models.presentation.list_widgets.RecipeListItem
import com.u343.cookingbook.book.models.presentation.main_screen.CategoriesAndRecipeViewModelData
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/**
 * [ViewModel] для фрагмента с рецептами и категориями
 *
 * @param interactor Интерактор главного экрана
 */
class CategoriesAndRecipesViewModel(
    private val interactor: MainScreenInteractor
) : ViewModel(), MainScreenObserverEvent {

    init {
        interactor.subscribeOnUpdates(this)
    }

    /**
     * [LiveData] с данными для отрисовки основного экрана с рецептами и категориями
     */
    private val _recyclerViewData = MutableLiveData<CategoriesAndRecipeViewModelData>()
    val recyclerViewData: LiveData<CategoriesAndRecipeViewModelData>
        get() = _recyclerViewData

    override fun event() {
        Log.d("bondarevTest", "CategoriesAndRecipesViewModel event")
        loadData()
    }

    fun loadData() {
        runBlocking {
            launch {
                createRecyclerViewData()
            }
        }
    }

    private suspend fun createRecyclerViewData() {
        val resultList = emptyList<ListItem>().toMutableList()

        val allCategories = interactor.getAllCategories().map { categoryData ->
            CategoryListItem(
                categoryId = categoryData.categoryId,
                categoryName = categoryData.categoryName,
                image = null,
                recipeCount = "0"
            )
        }

        val allRecipes = interactor.getAllRecipes().map { recipeData ->
            RecipeListItem(
                recipeId = recipeData.recipeId,
                recipeName = recipeData.recipeName,
                image = null,
                isFavorite = recipeData.isFavorite
            )
        }

        if (!allCategories.isNullOrEmpty()) {
            val categoriesCount = allCategories.size

            resultList += LabelListItem("Категории", categoriesCount.toString())

            resultList += if (categoriesCount > 3) {
                allCategories.slice(0..2) + LabelListItem("Еще", null)
            } else {
                allCategories
            }
        }

        if (!allRecipes.isNullOrEmpty()) {
            resultList += LabelListItem("Рецепты", allRecipes.size.toString())
            resultList += allRecipes
        }

        _recyclerViewData.value = CategoriesAndRecipeViewModelData(
            resultList,
            allRecipes.size,
            allCategories.size
        )
    }
}
