package com.u343.cookingbook.book.presentation.recipes.dialog

/**
 * Обработчик нажатий для диаога изменения категории
 *
 * Активити и интрфейсы, которые собираются использовать диалог "Изменение категории" должны
 * наследовать этот интерфейс
 */
interface ChangeCategoryListener {

    fun onChangeDialogItemClick(idCategory: Int)
}
